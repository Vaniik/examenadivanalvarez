var express = require('express');

var bodyParser = require('body-parser');

const fileType = require('file-type');


var app = express();

app.use(bodyParser.json());


app.get('/', function(req, res) {
    res.end();
});

// ---------------Especificacion 2-------------

app.post('/removeduplicatewords',function(req,res){
    if(req.body.palabras!=null){
        
        var strPalabras=[];
        var finalString='';
        var palabraTemp='';
        for(var x=0;req.body.palabras.length>x;x++){
            
            if(req.body.palabras[x]==','){
                
                var rep=false;
                for(var i=0;strPalabras.length>i;i++){
                    if(palabraTemp==strPalabras[i]){
                        rep=true;
                    }
                }
                if(!rep){
                    strPalabras.push(palabraTemp);
                    

                }
                palabraTemp='';
            }else{
                palabraTemp+=req.body.palabras[x];
                
            }
        }
        for(var x=0;strPalabras.length>x;x++){
                finalString+=strPalabras[x];
                if(x<strPalabras.length-1)
                {
                    finalString+=','
                }
        }
            //console.log(finalString);
        res.write(finalString);
    }else{
        
        res.write('Utiliza la variable JSON "palabras" para declarar la  cadena que quieres eliminar duplicados')
    }
    res.end();
});


//---------------Especificacion 3-----------------



app.post('/detectfiletype',function(req,res){
    
    if(req.body.url!=null){
        
        const http = require('http');
 
        const url = req.body.url;
        var stringJson;
        http.get(url, response => {
                        response.on('readable', () => {
                                            const chunk = response.read(fileType.minimumBytes);
                                            response.destroy();
                                            //console.log('file: ',fileType(chunk));
                                            stringJson=(JSON.stringify(fileType(chunk)));
                                            console.log('file:',stringJson);
                                            
                                        
                          
                                            
                                
                            });
                    });
        /*
        console.log("url valida");
        res.download();
        res.sendFile("/images");*/
    }
    //console.log(stringJson);
    
    //res.write('funciono');
    res.write('Lo hace pero no me lo guarda. Mira la consola del server');
    res.end()
});



//-----------------Especificacion 4---------------------

var botorders=[];

app.get('/botorder/:order',function(req,res){
    if(req.params.order!=null){
        console.log("Orden:",req.params.order);
        var actualBotOrder={orderName:req.params.order,botorder:'NONE'};
        for(var bot=0;botorders.length>bot;bot++){
            console.log("buscando order ...");
            if(botorders[bot].orderName==actualBotOrder.orderName){
                console.log("order encontrada");
                actualBotOrder.botorder=botorders[bot].botorder;
            }
        }
        //console.log( "orden:",actualBotOrder.botorder);
        res.write(actualBotOrder.botorder);
        
    }else{
        res.write("No Order, make sure you put the order on the url");
    }
    res.end()
});

app.post('/botorder/:order',function(req,res){
    if(req.params.order!=null){
        res.write('OK');
        var botOrderEncontrada=false;
        for(var bot=0;botorders.length>bot;bot++){
            if(botorders[bot].orderName==req.params.order){
                botorders[bot].botorder=req.body.botorder;
                botOrderEncontrada=true;
            }
        }
        if(!botOrderEncontrada){
            var newBotOrder={orderName:req.params.order,botorder:req.body.botorder};
            botorders.push(newBotOrder);
        }
    }
    else{
        res.write('NO ORDER SERIAL');
    }
    res.end();
});



app.listen(8000);
console.log('Listening port 8000');